<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Warehouse
Route::get('warehouse', 'WarehouseController@index');
Route::post('warehouse', 'WarehouseController@create');
//Bike
Route::get('bike', 'BikeController@index');
Route::get('bike', 'BikeController@filter');
Route::get('bike/{id}', 'BikeController@show');
Route::put('bike/{id}', 'BikeController@update');
Route::post('bike', 'BikeController@create');
//Order
Route::get('orders', 'OrderController@index');
Route::get('orders/{id}', 'OrderController@show');
Route::post('orders', 'OrderController@store');
Route::put('orders/{id}', 'OrderController@update');
//Extra
Route::get('extras', 'ExtraController@index');
Route::get('extras/{id}', 'ExtraController@show');
Route::post('extras', 'ExtraController@store');
Route::put('extras/{id}', 'ExtraController@update');
