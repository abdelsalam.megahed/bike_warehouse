<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model
{
    //For put endpoint
    protected $fillable = ['warehouse_id'];

    public function order()
    {
        return $this->hasOne('App\Order');
    }
}
