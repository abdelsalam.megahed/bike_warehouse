<?php

namespace App\Http\Controllers;

use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    public function index()
    {
        $warehouses = Warehouse::all();
        return $warehouses;
    }

    public function create(Request $request){
        $warehouse = new Warehouse;
        $warehouse->name = $request->input('name');

        if($warehouse->name != null){
            $warehouse->save();
        }else{
            return response()->json(["Warehouse name is invalid"]);
        }

        return response()->json(["success", true]);
    }
}
